'use strict';
 
const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const watch = require('gulp-watch');
 
gulp.task('sass-compile', function () {
  return gulp.src('./wp-content/themes/radevs/sass/**/*.scss')
	.pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
	.pipe(sass({outputStyle: 'compressed'}))
	.pipe(sourcemaps.write('./public/'))
    .pipe(gulp.dest('./wp-content/themes/radevs/'))
})
 
gulp.task('watch', function () {
  gulp.watch('./wp-content/themes/radevs/sass/**/*.scss', gulp.series('sass-compile'))
})