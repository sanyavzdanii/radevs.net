<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://ru.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'radevs' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', '' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '[A}|fU~j*(Gt82wo5Zy7XoRecpiuNq~[ZjEd|VT.//sWvQf8w #C411#Tiqfft&~' );
define( 'SECURE_AUTH_KEY',  'V3Whf2E.0sPNqJ^JD8T6K6Ox<nZUvA7S9eAHxK.-BE_dI#>[;Tpyu`w2i)s2Bo<q' );
define( 'LOGGED_IN_KEY',    'L^Vu)P~gekeJ .qwLUE>c.$R@K0e]EI?>3vMO<Z8wD>;Y3:}du3|,YuqK.[3P!^=' );
define( 'NONCE_KEY',        'ghQ]*O;mXmti1cgcC6m6xo6n eJ;^C{b4JX&FolW!mKltYHN9m^5Of-tAf_z{I$o' );
define( 'AUTH_SALT',        'MUm9e``D_i@[(/qY5Ap[Bg0f*keapVk,V7XDu me!Z2ceV}~KL0HR]}S#tx/T:)g' );
define( 'SECURE_AUTH_SALT', '/=G5b4h<>>PRQ`*CkH1ZFc}>wk&2 Pd{J:-jQYv FtC!w_}KEetbtkhrrxH F(f`' );
define( 'LOGGED_IN_SALT',   'zQ`4f!R2`M}6f;h<x8(x^Eam?,<g/C-beW>vM)<}TNQ]N81T~)<mIh0zNOID?Je~' );
define( 'NONCE_SALT',       'W@@2`tbE>1SsmTLtoCZ?3b;e]#!E2fT[]HCXfMhMAyiEp|$U073q<5r>#qA+^wb?' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в документации.
 *
 * @link https://ru.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once ABSPATH . 'wp-settings.php';
