<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package radevs
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&display=swap" rel="stylesheet">
	
	<link rel="stylesheet" type="text/css" href="/slick/slick.css"/>
	<link rel="stylesheet" type="text/css" href="/slick/slick-theme.css"/>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site container">
<div class="row">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'radevs' ); ?></a>

	<header id="masthead" class="site-header span span_12">
		<div class="row">
			<div class="span span_4 paddingleft">
				<?php 

				$phone = get_field('phone','options');

				if( !empty($phone) ): ?>

					<a href="tel:<?php echo $phone; ?>" class="button_call textright"><p><?php echo $phone; ?></p></a>

				<?php endif; ?>
				<?php 

				$email = get_field('email','options');

				if( !empty($email) ): ?>

					<a href="mailto:<?php echo $email; ?>" class="button_call textright"><p><?php echo $email; ?></p></a>

				<?php endif; ?>
				<?php get_search_form(); ?>
			</div><div class="site-branding span span_4 site-branging_logo">
				<?php 

				$image = get_field('logo','options');

				if( !empty($image) ): ?>

					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="site-branging_logo" />

				<?php endif; ?>
			</div><!-- .site-branding --><div class="span span_4 textright">
				<a href="<?php echo esc_url( wc_get_cart_url() ); ?>"><button class="btn btn_cart">3 items <i class="btn_cart_icon"></i></button></a>
				<a href="<?php echo wp_login_url( get_permalink() ); ?>"><button class="btn btn_log_in">Log in <i class="btn_log_in_icon"></i></button></a>
			</div>
		</div>
		<nav id="site-navigation" class="main-navigation row">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'radevs' ); ?></button>
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				)
			);
			?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->
	
