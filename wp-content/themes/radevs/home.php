<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * Template Name: Popular Products
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package radevs
 */

get_header();
?>

	<main id="primary" class="site-main span span_12">
		<div class="site-main__header">
			<div class="row">
				<div class="span span_2 textright"><span class="span stick"></div></span><div class="span span_10"><?php the_title( '<h1 class="site-main__header_text span">', '</h1>' ); ?></div>
			</div>
		</div>
		<div class="site-main_popular_slider">
		  <?php 
		  $products = get_field('популярные','options');
		  foreach ($products as $key => $value) {
		  ?>
		  <div class="popular_slider__product"><?php echo get_the_post_thumbnail( $value, 'medium'); ?><div class="heart"></div>
		  <?php 
			$_product = wc_get_product( $value );
			$average      = $_product->get_average_rating();

			echo '<div class="star-rating">
					<span style="width:'.( ( $average / 5 ) * 100 ) . '%" title="'. 
					  $average.'">
						<strong itemprop="ratingValue" class="rating">'.$average.'</strong> '.__( 'out of 5', 'woocommerce' ).                              
					'</span></div></a>';
		  ?>
		  <hr>
		  
		  <a href="<?php echo get_permalink( $value ); ?>"><h2><?php echo get_the_title( $value ); ?></h2></a>
		  <div class="row"><div class="span span_6">Starts at</div><div class="span span_6 textleft popular_slider__product_price"><?php 
			echo $_product->get_price();
		  ?> $</div></div>
		  </div>
		  <?php 
		  //var_dump($_product);
		  }
		  ?>
		</div>
		<script type="text/javascript">
			jQuery(document).ready(function(){
			  jQuery('.site-main_popular_slider').slick({
				dots: true,
				  infinite: false,
				  speed: 300,
				  slidesToShow: 4,
				  slidesToScroll: 4,
				  responsive: [
					{
					  breakpoint: 1024,
					  settings: {
						slidesToShow: 3,
						slidesToScroll: 3,
						infinite: true,
						dots: true
					  }
					},
					{
					  breakpoint: 600,
					  settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					  }
					},
					{
					  breakpoint: 480,
					  settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					  }
					}
					// You can unslick at a given breakpoint now by adding:
					// settings: "unslick"
					// instead of a settings object
				  ]
				});
			});
		 </script>
		

	</main><!-- #main -->
	<div class="top_banner">Announcement Announcement Announcement Announcement  Announcement <span class="top_banner__offer_text">Special Offers<span>X</span></span></div>
<?php
get_footer();
